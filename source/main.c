#include <stdio.h>
#include "dice.h"

int main(){
	initializeSeed();

	int faces;
	printf("Type the number of faces the dice will have: ");
	scanf("%d", &faces);

	printf("Let's roll the dice: %d\n", rollDice(faces));
	return 0;
}
